import xml.etree.ElementTree as ET
from unittest.mock import patch

from gitlab_ci_scripts.pycov_convert_relative_filenames import convert

DUMMY_INPUT = r"""<?xml version="1.0" ?>
<coverage version="6.4.4" timestamp="1664387988822" lines-valid="30" lines-covered="0" line-rate="0" branches-covered="0" branches-valid="0" branch-rate="0" complexity="0">
	<!-- Generated by coverage.py: https://coverage.readthedocs.io -->
	<!-- Based on https://raw.githubusercontent.com/cobertura/web/master/htdocs/xml/coverage-04.dtd -->
	<sources>
		<source>/home/example/src/project/package</source>
	</sources>
	<packages>
		<package name="." line-rate="0" branch-rate="0" complexity="0">
			<classes>
				<class name="__main__.py" filename="__main__.py" complexity="0" line-rate="0" branch-rate="0">
					<methods/>
					<lines>
						<line number="3" hits="0"/>
					</lines>
				</class>
			</classes>
		</package>
	</packages>
</coverage>
"""


@patch("os.getcwd", return_value="/home/example/src/project")
def test_main(*_args):
    output = convert(DUMMY_INPUT)

    root = ET.fromstring(output)
    sources = root.findall("./sources/source")
    assert len(sources) == 1, "Expecting a single './sources/source' element"
    assert sources[0].text == "/home/example/src/project"

    packages = root.findall("./packages/package")
    assert len(packages) == 1
    assert packages[0].attrib["name"] == "package"

    klass = root.findall("./packages/package[1]/classes/class")
    assert len(klass) == 1
    assert klass[0].attrib["filename"] == "package/__main__.py"
